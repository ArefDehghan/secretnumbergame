using System;

namespace SecretNumber.Infrastructure
{
    public class Game
    {
        private int _secretNumber;

        public void SetSecretNumber(int start = 0, int end = 100)
        {
            var radnom = new Random();
            _secretNumber = radnom.Next(start, end + 1);
        }

        public GameResult GetGameResult(int input)
        {
            if (input == _secretNumber)
                return GameResult.Correct;
            else if (input < _secretNumber)
                return GameResult.Bigger;
            else 
                return GameResult.Smaller;
        }
    }
}
