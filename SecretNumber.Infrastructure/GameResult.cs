namespace SecretNumber.Infrastructure
{
    public enum GameResult
    {
        Correct,
        Bigger,
        Smaller
    }
}
