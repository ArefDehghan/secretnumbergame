namespace SecretNumber.Infrastructure
{
    public class GameRunner
    {
        private IO _io;
        private Game _game;
        public GameRunner(IO io, Game game)
        {
            _io = io;
           _game = game; 
        }

        public void NewSecretNumber()
        {
            _game.SetSecretNumber();
        }

        public void StartGame()
        {
            var answer = _io.GetUserAnswer();
            var result = _game.GetGameResult(answer);
            _io.PrintResult(result);

            if (result == GameResult.Correct)
                NewSecretNumber();    
        }
    }
}