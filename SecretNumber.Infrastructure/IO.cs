namespace SecretNumber.Infrastructure
{
    public interface IO
    {
        void PrintResult(GameResult gameResult);
        int GetUserAnswer();
    }
}