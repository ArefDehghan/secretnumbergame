using System;
using SecretNumber.Infrastructure;

namespace SecretNumber.UI.Cli
{
    class IOConsole : IO
    {
        public int GetUserAnswer()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Answer: ");
            return int.Parse(Console.ReadLine());
        }

        public void PrintResult(GameResult result)
        {
            if (result == GameResult.Correct)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Thats it!");
            }
            else if (result == GameResult.Smaller)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Go down!");
            }
            else if (result == GameResult.Bigger)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Go up!");
            }
        }
    }
}