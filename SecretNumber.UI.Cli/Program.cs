﻿using System;
using SecretNumber.Infrastructure;

namespace SecretNumber.UI.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var gameRunner = new GameRunner(new IOConsole(), new Game());
            gameRunner.NewSecretNumber();
            while (true)
            {
                gameRunner.StartGame();
            }
        }
    }
}
