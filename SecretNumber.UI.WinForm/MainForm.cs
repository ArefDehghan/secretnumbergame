﻿using System.Drawing;
using System.Windows.Forms;
using SecretNumber.Infrastructure;

namespace SecretNumber.UI.WinForm
{
    public partial class MainForm : Form, IO
    {
        private GameRunner _gameRunner;

        public MainForm()
        {
            InitializeComponent();

            _gameRunner = new GameRunner(this, new Game());
            _gameRunner.NewSecretNumber();
        }

        public int GetUserAnswer()
        {

            return int.Parse(AnswerInput.Text);
        }

        public void PrintResult(GameResult gameResult)
        {
            if (gameResult == GameResult.Correct)
            {
                ResultText.ForeColor = Color.SeaGreen;
                ResultText.Text = $"Thats it!";
            }
            else if (gameResult == GameResult.Smaller)
            {
                ResultText.ForeColor = Color.Maroon;
                ResultText.Text = "Go down!";
            }
            else if (gameResult == GameResult.Bigger)
            {
                ResultText.ForeColor = Color.Gold;
                ResultText.Text = "Go up!";
            }
        }

        private void AnswerInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void SubmitButton_Click(object sender, System.EventArgs e)
        {
            _gameRunner.StartGame();
            AnswerInput.Text = string.Empty;
        }
    }
}
